<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductosController extends Controller
{
    public function productosRegistrados()
    {
        return view("productosAdmin");
    }

    public function addProductos()
    {
        return view("addProductosAdmin");
    }

    public function registrarProducto(Request $datos)
    {

        if (!$datos->nombreProducto || !$datos->precio || !$datos->descripcion) {

            return view("addProductosAdmin", ["estatus" => "error", "mensaje" => "¡Falta información!"]);

        } else {

            //imagen 1
            $datos->validate([
                'img1' => 'required|image|max:4000'
            ]);
            $datos->validate([
                'img2' => 'required|image|max:4000'
            ]);
            $datos->validate([
                'precio' => 'numeric|min:1|max:10'
            ]);

            $img1 = $datos->file('img1')->store('public/productos');
            $img2 = $datos->file('img2')->store('public/productos');
            $url1 = Storage::url($img1);
            $url2 = Storage::url($img2);


            $producto = new Producto();
            $producto->nombre_producto = $datos->nombreProducto;
            $producto->precio = $datos->precio;
            $producto->descripcion = $datos->descripcion;
            $producto->ruta_imagen1 = $url1;
            $producto->ruta_imagen2 = $url2;
            $producto->save();

            return view("addProductosAdmin", ["estatus" => "success", "mensaje" => "¡Producto agregado exitosamente!"]);


        }
    }


}

