<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\ProductosController;


Route::get('/', function () {
    return view('inicio');
});
Route::get('/inicio',[UsuarioController::class,'inicio'])->name('inicio');
Route::get('/login',[UsuarioController::class,'login'])->name('login');
Route::post('/login',[UsuarioController::class,'verificarCredenciales'])->name('login.form');
Route::get('/registro',[UsuarioController::class,'registro'])->name('registro');
Route::post('/registro',[UsuarioController::class,'registroForm'])->name('registro.form');
Route::get('/cerrarSesion',[UsuarioController::class,'cerrarSesion'])->name('cerrar.sesion');

Route::prefix('/usuario')->middleware("VerificarUsuario")->group(function () {

    //Route user
    Route::get('/menu', [UsuarioController::class, 'menu'])->name('usuario.menu');


    //Route admin
    Route::get('/menuAdmin', [UsuarioController::class, 'menuAdmin'])->name('usuario.menu.admin');
    Route::get('/productosRegistrados', [ProductosController::class, 'productosRegistrados'])->name('productos.admin');
    Route::get('/addProducto', [ProductosController::class, 'addProductos'])->name('productos.admin.add');
    Route::post('/addProductos', [ProductosController::class, 'registrarProducto'])->name('registrar.productos.admin');
});
