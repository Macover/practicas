<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\RespuestaController;

Route::get('/', function () {
    return view('inicio');
});

Route::get('/inicio',[UsuarioController::class,'inicio'])->name('inicio');
Route::get('/login',[UsuarioController::class,'login'])->name('login');
Route::post('/login',[UsuarioController::class,'verificarCredenciales'])->name('login.form');
Route::get('/registro',[UsuarioController::class,'registro'])->name('registro');
Route::post('/registro',[UsuarioController::class,'registroForm'])->name('registro.form');
Route::get('/cerrarSesion',[UsuarioController::class,'cerrarSesion'])->name('cerrar.sesion');

Route::prefix('/usuario')->middleware("VerificarUsuario")->group(function () {

    //Route user
    Route::get('/menu', [UsuarioController::class, 'preguntasForm'])->name('usuario.menu');
    Route::post('/menu', [RespuestaController::class, 'verificarRespuestas'])->name('verifica.respuestas');


    //Route admin
    Route::get('/menuAdmin', [UsuarioController::class, 'menuAdmin'])->name('usuario.menu.admin');

});
