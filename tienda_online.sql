-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-03-2021 a las 21:23:19
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tienda_online`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id_producto` int(11) NOT NULL,
  `nombre_producto` varchar(50) NOT NULL,
  `precio` int(12) NOT NULL,
  `descripcion` text NOT NULL,
  `ruta_imagen1` varchar(100) NOT NULL,
  `ruta_imagen2` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id_producto`, `nombre_producto`, `precio`, `descripcion`, `ruta_imagen1`, `ruta_imagen2`, `created_at`, `updated_at`) VALUES
(3, 'adasd', 3434, 'asdasd', '/storage/productos/RxnIt8tWenbiQElQ5hEgoaBM35HMv1tsmMpW9pn1.png', '/storage/productos/RxnIt8tWenbiQElQ5hEgoaBM35HMv1tsmMpW9pn1.png', '2021-03-29 01:21:09', '2021-03-29 01:21:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `nombre_usuario` varchar(50) NOT NULL,
  `correo` varchar(70) NOT NULL,
  `contrasenia` varchar(80) NOT NULL,
  `tipo_usuario` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nombre_usuario`, `correo`, `contrasenia`, `tipo_usuario`, `created_at`, `updated_at`) VALUES
(1, 'isai', 'isai@12', '$2y$10$tpAnabfYx24P//N0jjDFpe8ZyB4zeXqxd8EaNn.P3fvvjui20p8na', 'user', '2021-03-27 20:41:31', '2021-03-27 20:41:31'),
(2, 'isai1', 'isai@123', '$2y$10$jqOU3JgE9vx1NcMtl2//1eom/19HHaqDVVzbDG2aFBCeWxfrkP2i6', 'user', '2021-03-27 20:53:14', '2021-03-27 20:53:14'),
(3, '123', '123@123', '$2y$10$BL1iyDiWdA.r.ow5a3tPHusTX4CDgryFUPQNJt.TZ80zrVN4ob126', 'user', '2021-03-27 20:55:11', '2021-03-27 20:55:11'),
(4, '1234', '1234@1234', '$2y$10$OAHVQZNhsBARuy0fobrsrO20TmTwH7xrK6Qu/kWXPkMHSRi9RGTLq', 'user', '2021-03-27 21:16:44', '2021-03-27 21:16:44'),
(5, 'root', 'admin@root', '$2y$10$vk9QQ.c0CaJZpEuwPLx0v.IsNCWz/msXcZCxyAInKLv2yERkIqZ0G', 'admin', '2021-03-27 23:34:13', '2021-03-27 23:34:13'),
(6, '12345', '12345@12345', '$2y$10$COnRFyoAlVUSVo93Fma/3eJLz8bde3S3lLoED9rs9ObpdC5qT09Ai', 'user', '2021-03-28 00:19:00', '2021-03-28 00:19:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id_producto`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
